<?php

namespace App\Http\Controllers\Front;

use App\Donatur;
use App\Http\Controllers\Controller;
use App\Pengkhususan;
use App\Rekening;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index(){
        $data['rekening'] = Rekening::where('id','!=',1)->get();
        return view('frontend.donasi', $data);
    }




    public function getPengkhususanList(Request $request)
    {
//        dd($request->all());
        $input = $request->all();

        $data = Pengkhususan::where('is_deleted', 0)->orderBy('id');
        if (@$input['search']) $data = $data->where('pengkhususan', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page']-1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->jenis->jenis.' - '.$row->pengkhususan,
            ];
        }

        return ['results' => $return, 'pagination' => ['more' => $more]];
    }

}
