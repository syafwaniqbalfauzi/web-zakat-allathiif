<?php

namespace App\Http\Controllers\Front;

use App\Donatur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DonaturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.new-donatur');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.new-donatur');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDonaturList(Request $request)
    {
        $input = $request->all();

        $data = Donatur::orderBy('id');
        if (@$input['search']) $data = $data->where('nama', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page'] - 1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->nama.' - '.$row->alamat
            ];
        }
        return ['results' => $return, 'pagination' => ['more' => $more]];
    }

    public function getDonatur($id){
        $donatur = Donatur::where('id', $id)->first();
        return response()->json(['data' => $donatur]);
    }
}
