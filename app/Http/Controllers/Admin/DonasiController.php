<?php

namespace App\Http\Controllers\Admin;

use App\Donasi;
use App\DonasiDetail;
use App\Http\Controllers\Controller;
use App\Jenis;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DonasiController extends Controller
{

    public function index()
    {
        return view('backend.pages.donasi.index');
    }

    public function getAllDonasi(Request $request){

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];

        $data = Donasi::with('donasi_detail','donasi_detail.pengkhususan')->where('is_deleted',0)
            ->orderBy('tanggal_donasi', 'DESC');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) AND !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('id', 'like', '%' . $search['value'] . '%');
                $query->orWhere('kode_verifikasi', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nama', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);

        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;

            $d[] = $row->kode_verifikasi;
            $d[] = $row->donatur->nama;
            $d[] = $row->donatur->telepon;
            $d[] = $row->rekening->nama_bank;
            $detail = '';
            foreach($row->donasi_detail as $val){
                $detail .= $val->pengkhususan->pengkhususan.' - '.$val->nominal.'<br>';
            }
            $d[] = $detail;
            $d[] = 'Rp. '.$row->total_nominal;

            if($row->transfer == 0){
                $d[] = '<span class="badge badge-success">'."Pembayaran Langsung".' </span>';
            }else if ($row->transfer == 1){
                $d[] = '<span class="badge badge-warning">'."Menunggu Transfer".' </span>';
            }else if ($row->transfer == 2){
                $d[] = '<span class="badge badge-success">'."Transfer Terkonfirmasi".' </span>';
            }
            $btn = '<a href="' . route('donasi.edit', $row->id) . '" class="btn  btn-warning" style="margin-right: 3px;">  <i class="far fa-edit"></i></a>';
            if($row->transfer != 0){
                $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-success btn-bukti " style="margin-right: 3px;">  <i class="fas fa-file-image"></i></a>';
            }
            $btn .='<div class="btn-group" style="margin-right: 3px;">
                      <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Status
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item btn-status" data-id="'.$row->id.'" data-status="0" href="#" >Pembayaran Langsung</a>
                        <a class="dropdown-item btn-status"  data-id="'.$row->id.'" data-status="1" href="#">Menunggu Konfirmasi</a>
                        <a class="dropdown-item btn-status"  data-id="'.$row->id.'" data-status="2" href="#">Terkonfirmasi</a>
                      </div>
                    </div>';
            $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-danger btn-del " >  <i class="fas fa-trash-alt"></i></a>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);
            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }


    public function create()
    {
        $data['jenis'] = Jenis::all();
        return view('backend.pages.donasi.form',$data);
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $rules = [
            'donatur_id' => 'required',
            'tanggal_donasi' => 'required',
            'transfer' => 'required',
        ];
        $customMessages = [
            'donatur_id.required' => 'Field Donatur Wajib Diisi!',
            'tanggal_donasi.required' => 'Field Tanggal Donasi Wajib Diisi!',
            'transfer.required' => 'Field Status Donatur Wajib Diisi!'
        ];
        $this->validate($request, $rules, $customMessages);

        $input = $request->all();
        $randomString = Str::random(10);
        if ($request->hasFile('bukti_transfer')) {
            $input['bukti_transfer'] = $this->uploadImage($request,$randomString);
        }

        $input['kode_verifikasi'] = Str::random(20);
        $input['total_nominal'] = 0;
        $donasi = Donasi::create($input);

        $nominal = 0;
        for ($i = 1; $i <= count($input['khusus']); $i++) {
            $st = [
                'donasi_id' => $donasi->id,
                'pengkhususan_id' => $input['khusus'][$i - 1]['pengkhususan_id'],
                'nominal' => $input['khusus'][$i - 1]['nominal']
            ];
            $detail = DonasiDetail::create($st);
            $nominal += $input['khusus'][$i - 1]['nominal'];
        }

        if ($detail){
            $donasi = Donasi::find($donasi->id);
            $donasi->total_nominal = $nominal;
            $status = $donasi->save();
        }

        if ($status) {
            return redirect()->route('donasi.index')->with('success', 'Data berhasil ditambah');
        }

    }

    public function update(Request $request, $id)
    {
        $donasi = Donasi::find($id);
        $donasi->donatur_id = $request->donatur_id;
        $donasi->transfer = $request->transfer;
        $donasi->rekening_id = $request->rekening_id;
        $donasi->tanggal_donasi = $request->tanggal_donasi;
        $donasi->tanggal_transfer = $request->tanggal_transfer;
        $donasi->total_nominal = $request->total_nominal;

        $randomString = Str::random(10);
        if ($request->hasFile('bukti_transfer')) {
            $donasi->bukti_transfer = $this->uploadImage($request,$randomString);
        }
        $donasi->update();
        $detail = DonasiDetail::where('donasi_id', $id)->get();
        foreach($detail as $row){
            $row->delete();
        }

        $nominal = 0;
        for ($i = 1; $i <= count($request['khusus']); $i++) {
            $st = [
                'donasi_id' => $donasi->id,
                'pengkhususan_id' => $request['khusus'][$i - 1]['pengkhususan_id'],
                'nominal' => $request['khusus'][$i - 1]['nominal']
            ];
            $detail = DonasiDetail::create($st);
            $nominal += $request['khusus'][$i - 1]['nominal'];
        }

        if ($detail){
            $donasi = Donasi::find($donasi->id);
            $donasi->total_nominal = $nominal;
            $status = $donasi->save();
        }

        if ($status) {
            return redirect()->route('donasi.index')->with('success', 'Data berhasil diubah');
        }

    }



    public function edit($id)
    {
        $donasi = Donasi::where('id',$id)->with('donatur','rekening','donasi_detail','donasi_detail.pengkhususan')->first();

        $data['donasi'] = $donasi;
        return view('backend.pages.donasi.form',$data);
    }


    public function destroy($id)
    {
        $donasi = Donasi::find($id);
        $donasi->is_deleted = 1;
        $status = $donasi->save();

        $donasi_detail = DonasiDetail::where('donasi_id',$id)->get();
        foreach($donasi_detail as $row){
            $row->is_deleted = 1;
            $row->save();
        }
        if ($status) {
            return 1;
        } else {
            return 0;
        }
    }

    private function uploadImage(Request $request,$name){
        $image = $request->file('bukti_transfer');
        $ext = $image->getClientOriginalExtension();

        if($request->file('bukti_transfer')->isValid()){
            $upload_path = 'assets/uploads/donasi';
            $imagename = $upload_path.'/'.$name.'.'.$ext;
            $request->file('bukti_transfer')->move($upload_path,$imagename);
            return $imagename;
        }
        return false;
    }

    public function gantiStatus(Request $request){
        $donasi = Donasi::find($request->id);
        $donasi->transfer = $request->status;
        $status = $donasi->save();
        if ($status){
            return response(1);
        }
    }

    public function downloadBukti(Request $request){
        $donasi = Donasi::find($request->id);
        dd($donasi->bukti_transfer);
        $filepath = public_path($donasi->bukti_transfer);
        return response()->download($filepath);
    }
}
