<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rekening;
use Illuminate\Http\Request;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.master.rekening.index');
    }

    public function getAllRekening(Request $request){

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];

        $data = Rekening::where('is_deleted',0);

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) AND !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('id', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nama_bank', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);

        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;

            $d[] = $row->nama_bank;
            $d[] = $row->kode_bank;
            $d[] = $row->no_rekening;
            $d[] = $row->atas_nama;
            $btn = '<a href="' . route('rekening.edit', $row->id) . '" class="btn  btn-primary" style="margin-right: 3px;">  <i class="far fa-edit"></i></a>';
            $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-danger btn-del " >  <i class="fas fa-trash-alt"></i></a>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);
            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.master.rekening.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0)
    {
//        dd($request->all());
        $rules = [
            'nama_bank' => 'required',
            'kode_bank' => 'required',
            'no_rekening' => 'required',
            'atas_nama' => 'required'
        ];
        $customMessages = [
            'nama_bank.required' => 'Field Nama Bank Wajib Diisi!',
            'kode_bank.required' => 'Field Kode Bank Wajib Diisi!',
            'no_rekening.required' => 'Field Nomor Rekening Wajib Diisi!',
            'atas_nama.required' => 'Field Atas Nama Wajib Diisi!',
        ];
        $this->validate($request, $rules, $customMessages);

        $input = $request->all();
        if ($request->method() == "PATCH") {
            $rekening = Rekening::find($id);
            $status = $rekening->update($input);
        } else {
            $status = Rekening::create($input);
        }
        if ($status) {
            return redirect()->route('rekening.index')->with('success', 'Data berhasil ' . (empty($id) ? 'ditambah' : 'diubah'));
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['rekening'] = Rekening::find($id);
        return view('backend.pages.master.rekening.form',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekening = Rekening::find($id);
        $rekening->is_deleted = 1;
        $status = $rekening->save();

        if ($status) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getRekeningList(Request $request)
    {
        $input = $request->all();

        $data = Rekening::orderBy('id');
        if (@$input['search']) $data = $data->where('nama_bank', 'like', '%' . $input['search'] . '%')
            ->where('no_rekening', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page'] - 1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->nama_bank .' - '.$row->kode_bank.' '.$row->no_rekening ,
            ];
        }

        return ['results' => $return, 'pagination' => ['more' => $more]];
    }
}
