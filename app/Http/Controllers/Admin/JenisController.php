<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jenis;
use App\Pengkhususan;
use Illuminate\Http\Request;

class JenisController extends Controller
{

    public function index()
    {
        return view('backend.pages.master.jenis.index');
    }

    public function getAllJenis(Request $request){

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];

        $data = Jenis::where('is_deleted',0)->with('pengkhususan');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) AND !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('id', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nama_bank', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);

        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;

            $d[] = $row->jenis;
            $d[] = $row->keterangan;

            $jenis = '';
            foreach($row->pengkhususan as $key=>$val){
                if ($val->is_deleted == 0){
                    $jenis .= '- '.$val->pengkhususan . '<br>';
                }
            }

            $d[] = $jenis;
            $btn = '<a href="' . route('jenis.edit', $row->id) . '" class="btn  btn-primary" style="margin-right: 3px;">  <i class="far fa-edit"></i></a>';
            $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-danger btn-del " >  <i class="fas fa-trash-alt"></i></a>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);
            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }


    public function create()
    {
        return view('backend.pages.master.jenis.form');
    }

    public function store(Request $request, $id = 0)
    {
//        dd($request->all());
        $rules = [
            'jenis' => 'required'
        ];
        $customMessages = [
            'jenis.required' => 'Field Jenis Donasi Wajib Diisi!'
        ];
        $this->validate($request, $rules, $customMessages);

        $input = $request->all();

        $jenis = Jenis::updateOrCreate(['id' => $id], $input);

        $detail = Pengkhususan::where('jenis_id', $jenis->id)->get();

        if ($detail){
            foreach($detail as $row){
                $row->is_deleted = 1;
                $row->save();
            }
        }

        for ($i = 1; $i <= count($input['khusus']); $i++) {
            $st = [
                'jenis_id' => $jenis->id,
                'pengkhususan' =>$input['khusus'][$i - 1]['pengkhususan']
            ];
            $pengkhususan = Pengkhususan::create($st);
        }

        if ($pengkhususan) {
            return redirect()->route('jenis.index')->with('success', 'Data berhasil ' . (empty($id) ? 'ditambah' : 'diubah'));
        }

    }



    public function edit($id)
    {
        $jenis = Jenis::where('id', $id)->with('pengkhususan')->first();
        foreach ($jenis->pengkhususan as $row) {
            if ($row->is_deleted == 0){
                $d[] = [
                    'pengkhususan' => $row->pengkhususan,
                ];
            }
        }
        $data['pengkhususan'] = $d;
        $data['jenis'] = $jenis;

        return view('backend.pages.master.jenis.form',$data);
    }


    public function destroy($id)
    {
        $jenis = Jenis::find($id);
        $jenis->is_deleted = 1;
        $status = $jenis->save();

        if ($status) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getJenisList(Request $request)
    {
        $input = $request->all();

        $data = Jenis::orderBy('id');
        if (@$input['search']) $data = $data->where('jenis', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page'] - 1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->jenis,
            ];
        }

        return ['results' => $return, 'pagination' => ['more' => $more]];
    }
}
