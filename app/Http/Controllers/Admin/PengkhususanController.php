<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jenis;
use App\Pengkhususan;
use App\Rekening;
use Illuminate\Http\Request;

class PengkhususanController extends Controller
{

    public function index()
    {
        return view('backend.pages.master.pengkhususan.index');
    }

    public function getAllKhusus(Request $request){

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];

        $data = Pengkhususan::with('jenis')->where('is_deleted',0);

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) AND !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('id', 'like', '%' . $search['value'] . '%');
                $query->orWhere('pengkhususan', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);

        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;

            $d[] = $row->jenis->jenis;
            $d[] = $row->pengkhususan;
            $btn = '<a href="' . route('pengkhususan.edit', $row->id) . '" class="btn  btn-primary" style="margin-right: 3px;">  <i class="far fa-edit"></i></a>';
            $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-danger btn-del " >  <i class="fas fa-trash-alt"></i></a>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);
            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }


    public function create()
    {
        $data['jenis'] = Jenis::all();
        return view('backend.pages.master.pengkhususan.form',$data);
    }

    public function store(Request $request, $id = 0)
    {
//        dd($request->all());
        $rules = [
            'jenis_id' => 'required',
            'pengkhususan' => 'required'
        ];
        $customMessages = [
            'jenis_id.required' => 'Field Jenis Donasi Wajib Diisi!',
            'pengkhususan.required' => 'Field Pengkhususan Donasi Wajib Diisi!'
        ];
        $this->validate($request, $rules, $customMessages);

        $input = $request->all();
        if ($request->method() == "PATCH") {
            $jenis = Pengkhususan::find($id);
            $status = $jenis->update($input);
        } else {
            $status = Pengkhususan::create($input);
        }
        if ($status) {
            return redirect()->route('pengkhususan.index')->with('success', 'Data berhasil ' . (empty($id) ? 'ditambah' : 'diubah'));
        }

    }



    public function edit($id)
    {
        $data['khusus'] = Pengkhususan::find($id);
        $data['jenis'] = Jenis::all();
        return view('backend.pages.master.pengkhususan.form',$data);
    }


    public function destroy($id)
    {
        $jenis = Pengkhususan::find($id);
        $jenis->is_deleted = 1;
        $status = $jenis->save();

        if ($status) {
            return 1;
        } else {
            return 0;
        }
    }
    public function getPengkhususanList(Request $request)
    {
//        dd($request->all());
        $input = $request->all();

        $data = Pengkhususan::where('is_deleted', 0)->orderBy('id');
        if (@$input['search']) $data = $data->where('pengkhususan', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page']-1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->jenis->jenis.' - '.$row->pengkhususan,
            ];
        }

        return ['results' => $return, 'pagination' => ['more' => $more]];
    }
}
