<?php

namespace App\Http\Controllers\Admin;

use App\Donasi;
use App\Donatur;
use App\Http\Controllers\Controller;
use App\Jenis;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DonaturController extends Controller
{

    public function index()
    {
        return view('backend.pages.donatur.index');
    }

    public function getAllDonatur(Request $request){

        if (!$request->ajax()) return response('Forbidden', 403);

        $input = $request->all();

        // DataTable Default
        $length = (int)@$input['length'] ?? 10;
        $start = (int)@$input['start'];
        $search = @$input['search'];
        $order = @$input['order'];
        $status = @$input['status'];

        $data = Donatur::where('is_deleted',0)->orderBy('id', 'ASC');

        $count = $data->count();
        $table = [];
        $table['recordsFiltered'] = $count;
        $table['recordsTotal'] = $count;

        // Search
        if (!empty($search) AND !empty($search['value'])) {
            $data = $data->where(function ($query) use ($search) {
                $query->orWhere('id', 'like', '%' . $search['value'] . '%');
                $query->orWhere('kode_verifikasi', 'like', '%' . $search['value'] . '%');
                $query->orWhere('nama', 'like', '%' . $search['value'] . '%');
            });

            $table['recordsFiltered'] = $data->count(); // Menghitung jumlah data yang ditemukan berdasarkan search. Di datatables nya nanti muncul Total xx From xx (Filtered from xxxx)
        }

        $data_tmp = $data->skip($start)->take($length);

        // Init
        $i = $start + 1;
        foreach ($data_tmp->get() as $row) {
            $d = [];

            $d[] = $i++;

            $d[] = $row->nama;
            $d[] = $row->email;
            $d[] = $row->telepon;
            $d[] = $row->alamat;
            $btn = '<a href="' . route('donatur.edit', $row->id) . '" class="btn  btn-primary" style="margin-right: 3px;">  <i class="far fa-edit"></i></a>';
            $btn .= '<a href="#" data-id="' . $row->id . '" class="btn btn-danger btn-del " >  <i class="fas fa-trash-alt"></i></a>';

            $d[] = sprintf('<span style="overflow: visible; position: relative; width: 110px;">%s</span>', $btn);
            $d["DT_RowId"] = ($i - 1) . '#' . $row['_id'];
            $table['data'][] = $d;
        }

        if (empty($table['data'])) {
            $table['recordsTotal'] = $count;
            $table['recordsFiltered'] = 0;
            $table['aaData'] = [];
        }

        return response()->json($table);
    }


    public function create()
    {
        return view('backend.pages.donatur.form');
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $rules = [
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'telepon' => 'required'
        ];
        $customMessages = [
            'nama.required' => 'Field Nama Donatur Wajib Diisi!',
            'email.required' => 'Field Email Donatur Wajib Diisi!',
            'alamat.required' => 'Field Jenis Donatur Wajib Diisi!',
            'telepon.required' => 'Field Jenis Donatur Wajib Diisi!'
        ];
        $this->validate($request, $rules, $customMessages);

        $input = $request->all();
        $status = Donatur::create($input);
        if ($status) {
            return redirect()->route('donasi.create')->with('success', 'Data berhasil ditambah');
        }

    }

    public function update(Request $request, $id)
    {
        $rules = [
            'nama' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'telepon' => 'required'
        ];
        $customMessages = [
            'nama.required' => 'Field Nama Donatur Wajib Diisi!',
            'email.required' => 'Field Email Donatur Wajib Diisi!',
            'alamat.required' => 'Field Alamat Donatur Wajib Diisi!',
            'telepon.required' => 'Field Jenis Donatur Wajib Diisi!'
        ];
        $this->validate($request, $rules, $customMessages);

        $donatur = Donatur::find($id);
        $input = $request->all();
        $status = $donatur->update($input);
        if ($status) {
            return redirect()->route('donatur.index')->with('success', 'Data berhasil diubah');
        }
    }

    public function edit($id)
    {
        $data['donatur'] = Donatur::find($id);
        return view('backend.pages.donatur.form',$data);
    }

    public function show($id)
    {
        $donatur = Donatur::where('id', $id)->first();
        return response()->json(['data' => $donatur]);
    }

    public function destroy($id)
    {
        $donatur = Donatur::find($id);
        $donatur->is_deleted = 1;
        $status = $donatur->save();

        if ($status) {
            return 1;
        } else {
            return 0;
        }
    }

    private function uploadImage(Request $request,$name){
        $image = $request->file('bukti_transfer');
        $ext = $image->getClientOriginalExtension();

        if($request->file('bukti_transfer')->isValid()){
            $upload_path = 'assets/uploads/donasi';
            $imagename = $upload_path.'/'.$name.'.'.$ext;
            $request->file('bukti_transfer')->move($upload_path,$imagename);
            return $imagename;
        }
        return false;
    }

    public function gantiStatus(Request $request){
        $donasi = Donasi::find($request->id);
        $donasi->transfer = $request->status;
        $status = $donasi->save();
        if ($status){
            return response(1);
        }
    }

    public function downloadBukti(Request $request){
        $donasi = Donasi::find($request->id);
        dd($donasi->bukti_transfer);
        $filepath = public_path($donasi->bukti_transfer);
        return response()->download($filepath);
    }

    public function getDonaturList(Request $request)
    {
        $input = $request->all();

        $data = Donatur::orderBy('id');
        if (@$input['search']) $data = $data->where('nama', 'like', '%' . $input['search'] . '%');

        $count = $data->count();

        if ($count > 10) $more = TRUE;
        else $more = FALSE;

        $data = $data->take(10)->skip(10 * ($input['page'] - 1))->get();
        $return = [];
        foreach ($data as $row) {
            $return[] = [
                'id' => $row->id,
                'text' => $row->nama.' - '.$row->alamat
            ];
        }

        return ['results' => $return, 'pagination' => ['more' => $more]];
    }

}
