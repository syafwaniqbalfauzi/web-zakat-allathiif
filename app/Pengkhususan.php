<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengkhususan extends Model
{

    //is_deleted 1 = di delete
    protected $table = "pengkhususan";

    protected $fillable = [
        'jenis_id','pengkhususan', 'keterangan', 'is_deleted'
    ];

    public function jenis(){
        return $this->belongsTo('App\Jenis','jenis_id','id');
    }

}
