<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonasiDetail extends Model
{
    protected $table = "donasi_detail";
    protected $fillable = [
        'donasi_id', 'pengkhususan_id', 'nominal',
        'is_deleted'
    ];

    public function pengkhususan(){
        return $this->hasOne('App\Pengkhususan','id','pengkhususan_id');
    }
}
