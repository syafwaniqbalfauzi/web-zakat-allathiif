<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $table = "donasi";

    protected $fillable = [
        'kode_verifikasi', 'total_nominal', 'donatur_id','rekening_id',
        'transfer','bukti_transfer', 'tanggal_donasi','tanggal_transfer',
        'is_deleted'
    ];



    public function donatur(){
        return $this->hasOne('App\Donatur', 'id','donatur_id');
    }

    public function donasi_detail(){
        return $this->hasMany('App\DonasiDetail','donasi_id','id');
    }

    public function rekening(){
        return $this->hasOne('App\Rekening','id','rekening_id');
    }
}
