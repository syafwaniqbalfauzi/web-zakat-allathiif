<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jenis extends Model
{

    //is_deleted 1 = di delete
    protected $table = "jenis";

    protected $fillable = [
        'jenis', 'keterangan', 'is_deleted'
    ];

    public function pengkhususan(){
        return $this->hasMany('App\Pengkhususan','jenis_id','id');
    }
}
