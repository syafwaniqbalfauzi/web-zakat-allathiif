<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekening extends Model
{
    protected $table = "rekening";

    protected $fillable = [
        'nama_bank', 'kode_bank', 'no_rekening',
        'atas_nama', 'keterangan'
    ];

    public function rekening(){
        return $this->hasMany('App\Donasi','rekening_id','id');
    }
}
