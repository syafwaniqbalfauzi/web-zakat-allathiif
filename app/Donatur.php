<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donatur extends Model
{
    protected $table = "donatur";

    protected $fillable = [
        'sapaan', 'nama', 'email','alamat',
        'telepon','is_deleted'
    ];

    public function donasi(){
        return $this->hasMany('App\Donasi', 'donatur_id','id');
    }
}
