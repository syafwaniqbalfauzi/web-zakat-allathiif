<?php

use Illuminate\Database\Seeder;
use App\Pengkhususan;
use Carbon\Carbon;

class PengkhususanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Fitrah',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Penghasilan',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Fidyah',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Maal Penghasilan',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Maal Emas & Perak',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Maal Simpanan',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 1,
            'pengkhususan' => 'Zakat Maal Perdagangan',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 2,
            'pengkhususan' => 'Infak Masjid',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Pengkhususan::create([
            'jenis_id' => 3,
            'pengkhususan' => 'Wakaf Uang',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
