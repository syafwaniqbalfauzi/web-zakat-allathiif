<?php

use Illuminate\Database\Seeder;
use App\Rekening;
use Carbon\Carbon;

class RekeningTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rekening::create([
            'nama_bank' => 'Kas Mesjid',
            'kode_bank' => '-',
            'no_rekening' => '-',
            'atas_nama' => 'Masjid Al-Lathiif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        Rekening::create([
            'nama_bank' => 'BJB Syariah',
            'kode_bank' => '(425)',
            'no_rekening' => '0010301067771',
            'atas_nama' => 'Masjid Al-Lathiif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        Rekening::create([
            'nama_bank' => 'Bank Syariah Mandiri',
            'kode_bank' => '(451)',
            'no_rekening' => '7160017001',
            'atas_nama' => 'DKM Masjid Al-Lathiif',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
