<?php

use Illuminate\Database\Seeder;
use App\Jenis;
use Carbon\Carbon;

class JenisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jenis::create([
            'jenis' => 'Zakat',
            'keterangan' => 'Donasi Zakat',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Jenis::create([
            'jenis' => 'Infak',
            'keterangan' => 'Donasi Infak',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        Jenis::create([
            'jenis' => 'Wakaf',
            'keterangan' => 'Donasi Wakaf',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
