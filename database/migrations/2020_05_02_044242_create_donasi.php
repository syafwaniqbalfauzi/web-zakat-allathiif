<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi', function (Blueprint $table) {
            $table->id();
            $table->string('kode_verifikasi');
            $table->integer('donatur_id');
            $table->date('tanggal_donasi');
            $table->date('tanggal_transfer')->nullable();
            $table->bigInteger('total_nominal');
            $table->integer('transfer')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->string('bukti_transfer')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi');
    }
}
