@extends('frontend.layouts.app')
@section('title','Dash')
@section('donasi','active')

@push('styles')
<style>

    .wizard-steps .wizard-step:before {

        background-color: #e3eaef
    }
    .wizard-steps .wizard-step.wizard-step-active {
        box-shadow: 0 2px 6px #27ae60;
        background-color: #27ae60;
        color: #fff
    }
    .wizard-steps .wizard-step.wizard-step-active:before {
        background-color: #27ae60;
        color: #fff
    }
    .wizard>.steps .current a:hover,
    .wizard>.steps .current a:active {
        background: #27ae60;
        color: #fff;
        cursor: default
    }
    .wizard .steps .current a{
        background-color: #27ae60 !important;
    }
    .wizard .steps .done a{
      background-color: rgba(39,174,96,0.5)
    }
    .wizard>.steps .done a:hover,
    .wizard>.steps .done a:active {
        background: #27ae60;
        color: #fff
    }
    .wizard .steps .hover a{
        background-color: rgba(39,174,96,0.5)
    }
    .wizard>.actions a:hover, .wizard>.actions a:active{
        background-color: rgba(39,174,96,0.5)

    }
    .wizard>.actions .disabled a{
        background-color: #eee !important;

    }

    .wizard>.actions a{
        background-color: #27ae60 !important;

    }
</style>
@endpush

@section('content')
<div class="container-fluid mt-5">
    <div class="row justify-content-center align-center">
        <div class="col-4">
            <h3>Siapkan Donasi Terbaikmu</h3>
        </div>
    </div>
    <div class="row justify-content-center mt-5 mb-3">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="card">
                <div class="card-header center">
                </div>
                <div class="card-body">
                    <form id="wizard_with_validation" method="POST">
                        <h3>Data Diri</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">

                                    <a href="{{url('fdonatur')}}" class="btn btn-sm btn-success "><i class="fa fa-plus"></i> Tambah Donatur</a>

                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="donatur_id">Nama Donatur</label>
                                    <select name="donatur_id" id="donatur_id" class="form-control" >
                                        @if(@$donasi)
                                            <option value="{{$donasi->donatur_id}}">{{$donasi->donatur->nama}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">No Telepon</label>
                                    <input type="number" class="form-control" name="telepon" id="telepon" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Email Donatur</label>
                                    <input type="text" class="form-control" name="email" id="email" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Alamat</label>
                                    <textarea name="alamat" id="alamat" class="form-control" readonly></textarea>
                                </div>
                            </div>
                        </fieldset>

                        <h3>Jenis Donasi</h3>
                        <fieldset>
                            <div class="pk-repeater repeater">
                                <div class="form-group">
                                    <div class="text-left">
                                        <button type="button" class="btn btn-sm btn-success" data-repeater-create type="button">
                                            <i class="fa fa-plus"></i> Tambah Jenis Donasi
                                        </button>
                                    </div>
                                </div>
                                <div data-repeater-list="khusus">
                                    <div data-repeater-item>
                                        <div class="form-group">
                                            <div class="row mt-0">
                                                <div class="col-7">
                                                    <div class="form-group">
                                                        <label for="pengkhususan_id">Pengkhususan</label>
                                                        <select name="pengkhususan_id" id="" class="form-control pengkhususan " >
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <label for="nominal">Nominal Donasi</label>
                                                        <input type="number" class="form-control  nominal" name="nominal" id="nominal" min="0" >
                                                    </div>
                                                </div>

                                                <div class="col-1">
                                                    <div class="form-group" style="margin-top: 30px">
                                                        <button type="button" class="btn btn-danger " data-repeater-delete><i class="fa fa-trash"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Total Donasi</label>
                                        <input type="text" class="form-control mask" name="total_nominal" id="total_nominal" readonly >
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h3>Metode Pembayaran</h3>
                        <fieldset>
                            <div class="form-group">
                                <div class="form-line">
                                    <h6>Tentukan Metode Pembayaran</h6>
                                    <p>Terdapat beberapa metode pembayaran donasi yang bisa Anda dapat gunakan, di antaranya:</p>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <div class="form-line">
                                    @foreach($rekening as $row)
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios{{$row->id}}"  value="{{$row->id}}">
                                            <label class="form-check-label" for="exampleRadios{{$row->id}}" style="font-size: 16px;font-weight: bold">
                                               {{$row->nama_bank}} - {{$row->kode_bank}} {{$row->no_rekening}} an. {{$row->atas_nama}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </fieldset>

                        <h3>Konfirmasi Donasi</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">First Name*</label>
                                    <label class="form-label"></label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Last Name*</label>
                                    <input type="text" name="surname" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Email*</label>
                                    <input type="email" name="email" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Address*</label>
                                    <textarea name="address" cols="30" rows="3" class="form-control no-resize"
                                    ></textarea>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label class="form-label">Age*</label>
                                    <input min="18" type="number" name="age" class="form-control" >
                                </div>
                                <div class="help-info">The warning step will show up if age is less than 18</div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('assets/backend/')}}/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- JS Libraies -->
    <script src="{{asset('assets/backend/')}}/bundles/jquery-steps/jquery.steps.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="{{asset('assets/backend/')}}/js/page/form-wizard.js"></script>
    <!-- Template JS File -->
    <script src="{{asset('assets/backend/')}}/js/jquery.repeater.min.js"></script>

    <script>
        $(document).ready(function (){

            $('.mask').mask('000.000.000.000.000,00', {reverse: false});

            $('#submit').on('click', function (){
                $('.mask').unmask();
            })

            $repeater = $('.repeater').repeater({
                show: function () {
                    $(this).slideDown();
                    $('.nominal').keyup( function (){
                        hitungNominal();
                    });
                    $('.select2-input').last().html('<select name="khusus[][pengkhususan_id]" id="" class="form-control pengkhususan "></select>');
                    $('.pengkhususan').select2({
                        placeholder: '- Pilih Pengkhususan -',
                        ajax: {
                            url: '{{ route('home-pengkhususan.getList') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                    page: params.page || 1,
                                    jenis_id: $('[name=jenis_id]').val(),
                                    _token: '{{ csrf_token() }}',
                                }
                                return query;
                            }
                        }
                    });
                },
                hide: function (el) {
                    $(this).slideUp(el);
                    $('.btn-hapus-repeater').on('click', function (){
                        setInterval(function(){
                            var nominal_list = $('.nominal');
                            total_nominal = 0;
                            for (let i = 0;i < nominal_list.length ;i++){
                                total_nominal+=parseInt($(nominal_list[i]).val());
                            }
                            $('#total_nominal').val(total_nominal)
                        }, 1);
                    });
                }
            });

            $('.btn-hapus-repeater').on('click', function (){
                setInterval(function(){
                    var nominal_list = $('.nominal');
                    total_nominal = 0;
                    for (let i = 0;i < nominal_list.length ;i++){
                        total_nominal+=parseInt($(nominal_list[i]).val());
                    }
                    $('#total_nominal').val(total_nominal)
                }, 1);
            });
            $('.nominal').keyup( function (){
                hitungNominal();
            });

            $('[name=donatur_id]').select2({
                theme: 'bootstrap4',
                placeholder: '- Pilih Nama -',

                ajax: {
                    url: '{{ url('home-donatur/getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });

            $(document).on('change','#donatur_id',function (e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax({
                    'type': 'get',
                    'url': '{{url('home-donatur/')}}/'+id,
                    success: function (res) {
                        var data = res.data;
                        $('#email').val(data.email);
                        $('#telepon').val(data.telepon);
                        $('#alamat').val(data.alamat);

                    }
                });
            });

            $('.pengkhususan').select2({
                placeholder: '- Pilih Pengkhususan -',
                ajax: {
                    url: '{{ route('home-pengkhususan.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            jenis_id: $('[name=jenis_id]').val(),
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });


            function hitungNominal(){
                var nominal_list = $('.nominal');
                var total_nominal = 0;

                for (let i = 0;i<nominal_list.length;i++){
                    console.log(nominal_list.length)
                    total_nominal += parseInt($(nominal_list[i]).val());
                }
                $('#total_nominal').val(total_nominal)
            }

        });
    </script>
@endpush
