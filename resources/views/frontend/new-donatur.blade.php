@extends('frontend.layouts.app')
@section('title','Dash')
@section('donasi','active')

@push('styles')
<style>

    .wizard-steps .wizard-step:before {

        background-color: #e3eaef
    }
    .wizard-steps .wizard-step.wizard-step-active {
        box-shadow: 0 2px 6px #27ae60;
        background-color: #27ae60;
        color: #fff
    }
    .wizard-steps .wizard-step.wizard-step-active:before {
        background-color: #27ae60;
        color: #fff
    }
    .wizard>.steps .current a:hover,
    .wizard>.steps .current a:active {
        background: #27ae60;
        color: #fff;
        cursor: default
    }
    .wizard .steps .current a{
        background-color: #27ae60 !important;
    }
    .wizard .steps .done a{
        background-color: rgba(39,174,96,0.5)
    }
    .wizard>.steps .done a:hover,
    .wizard>.steps .done a:active {
        background: #27ae60;
        color: #fff
    }
    .wizard .steps .hover a{
        background-color: rgba(39,174,96,0.5)
    }
    .wizard>.actions a:hover, .wizard>.actions a:active{
        background-color: rgba(39,174,96,0.5)

    }
    .wizard>.actions .disabled a{
        background-color: #eee !important;

    }

    .wizard>.actions a{
        background-color: #27ae60 !important;

    }
</style>
@endpush

@section('content')
<div class="container-fluid mt-5">
    <div class="row justify-content-center align-center">
        <div class="col-6">
            <h3>Daftarkan diri anda menjadi donatur</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        </div>
    </div>
    <div class="row justify-content-center mt-2 mb-3">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="card">
                <div class="card-header center">
                </div>
                <div class="card-body">
                    <form action="{{ route('donatur.store')}}"  method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="sapaan">Sapaan Donatur</label>
                                    <select name="sapaan" id="sapaan" class="form-control">
                                        <option value="Bapak" {{@$donatur->sapaan == 'Bapak' ? 'selected' : ''}}>Bapak</option>
                                        <option value="Ibu" {{@$donatur->sapaan == 'Ibu' ? 'selected' : ''}}>Ibu</option>
                                        <option value="Saudara" {{@$donatur->sapaan == 'Saudara' ? 'selected' : ''}}>Saudara</option>
                                        <option value="Saudari" {{@$donatur->sapaan == 'Saudari' ? 'selected' : ''}}>Saudari</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nama">Nama Donatur</label>
                                    <input type="text" class="form-control" name="nama" id="nama" value="{{old('nama',@$donatur->nama)}}">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">Email Donatur</label>
                                    <input type="email" class="form-control" name="email" id="email" value="{{old('email',@$donatur->email)}}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="telepon">No. Telepon Donatur</label>
                                    <input type="number" class="form-control" name="telepon" id="telepon" min="0" value="{{old('telepon',@$donatur->telepon)}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">Alamat Donatur</label>
                                    <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10">{{{old('alamat', @$donatur->alamat)}}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-button text-right">
                            <a href="{{url('donasi')}}" class="btn btn-warning">Kembali</a>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{asset('assets/backend/')}}/bundles/jquery-validation/dist/jquery.validate.min.js"></script>
    <!-- JS Libraies -->
    <script src="{{asset('assets/backend/')}}/bundles/jquery-steps/jquery.steps.min.js"></script>
    <!-- Page Specific JS File -->
    <script src="{{asset('assets/backend/')}}/js/page/form-wizard.js"></script>
    <!-- Template JS File -->
    <script src="{{asset('assets/backend/')}}/js/jquery.repeater.min.js"></script>

    <script>
        $(document).ready(function (){
        });
    </script>
@endpush
