<script>
    $(document).ready(function(){
        @if ($errors->any())
        @foreach ($errors->all() as $error)
        iziToast.warning({
            title: 'Warning',
            message: '{{ $error }}',
            position: 'topRight'
        });
        @endforeach
        @endif
        @if(Session::has('success'))
        iziToast.success({
            title: 'Berhasil',
            message: '{{ Session::get('success') }}',
            position: 'topRight'
        });
        @endif
        @if(Session::has('error'))
        iziToast.error({
            title: 'Gagal',
            message: '{{ Session::get('error') }}',
            position: 'topRight'
        });
        @endif
    });
</script>
