@extends('backend.layouts.app')
@section('title','Dash')
@section('jenis','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Data Jenis Donasi</li>

                        <li class="breadcrumb-item"><a href="#"></a></li>
                        {{--                        <li class="breadcrumb-item active"><a href="#">Data Jenis</a></li>--}}
{{--                        <li class="breadcrumb-item active" aria-current="page">Data</li>--}}
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Master Data Jenis Donasi</h4>
                                <div class="card-header-action">
                                    <a href="{{ route('jenis.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-jenis">
                                        <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Jenis Donasi</th>
                                            <th>Keterangan</th>
                                            <th>Pengkhususan</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#table-jenis').DataTable( {
                deferRender: true,
                serverSide: true,
                processing: true,
                stateSave: true,
                scrollX: false,
                ajax: {
                    url: '{!! route('jenis.get') !!}',
                    type: 'POST',
                    data: function (e) {
                        e._token = '{{ csrf_token() }}';
                        return e;
                    }
                }
            });

            $(document).on('click','.btn-del', function () {
                var id = $(this).data('id');
                swal({
                    title: 'Apakah Anda Yakin?',
                    text: 'Data yang telah dihapus tidak bisa dikembalikan!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "delete",
                                url: "{{ url('admin/jenis') }}/"+id,
                                data: {
                                    '_token' : '{{ csrf_token() }}'
                                },
                                success: function (response) {
                                    if(response == 1){
                                        swal('Data berhasil dihapus!', {
                                            icon: 'success',
                                        });
                                        setTimeout(() => {
                                            location.reload();
                                        }, 2000);
                                    }else{
                                        swal('Data gagal dihapus!', {
                                            icon: 'error',
                                        });
                                    }
                                }
                            });

                        } else {

                        }
                    });
            });
        });

    </script>

@endpush
