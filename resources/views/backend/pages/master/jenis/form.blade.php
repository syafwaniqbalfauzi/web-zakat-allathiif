@extends('backend.layouts.app')
@section('title','Dash')
@section('jenis','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item "><a href="{{route('jenis.index')}}">Data Jenis Donasi</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$jenis ? "Ubah" : "Tambah"}} Data Jenis Donasi</li>
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{@$jenis ? "Ubah" : "Tambah"}} Data Jenis Donasi</h4>
                            </div>

                            <div class="card-body">
                                <form action="{{@$jenis ? route('jenis.update', $jenis->id) : route('jenis.store')}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$jenis)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="form-group">
                                        <label for="jenis">Jenis Donasi</label>
                                        <input id="jenis" name="jenis" type="text" class="form-control"
                                           value="{{old('nama_bank',@$jenis->jenis)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="keterangan">Keterangan</label>
                                        <textarea class="form-control" name="keterangan" id="keterangan" cols="30" rows="10">{{old('kode_bank',@$jenis->keterangan)}}</textarea>
                                    </div>

                                    <hr>
                                    <h6 style="font-size: 16px">Pengkhususan Jenis Donasi</h6>
                                    <div class="pk-repeater repeater">
                                        <div class="form-group">
                                            <div class="text-left">
                                                <button type="button" class="btn btn-sm btn-primary" data-repeater-create type="button">
                                                    <i class="fa fa-plus"></i> Tambah Pengkhususan
                                                </button>
                                            </div>
                                        </div>

                                        <div data-repeater-list="khusus">
                                            <div data-repeater-item>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label class="control-label label-kd3" for="subtema">Pengkhususan Zakat</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <input type="text" name="pengkhususan" class="form-control"  placeholder="Masukan Jenis Pengkhususan" value="Default"/>
                                                        </div>

                                                        <div class="col-sm-1">
                                                            <button type="button" class="btn btn-danger btn-sm" data-repeater-delete><i class="fa fa-trash"></i></button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-button text-right">
                                        <a href="{{route('jenis.index')}}" class="btn btn-warning">Kembali</a>
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script src="{{asset('assets/backend/')}}/js/jquery.repeater.min.js"></script>

    <script>
        $(document).ready(function () {
            $repeater = $('.repeater').repeater({
                show: function () {
                    $(this).slideDown();
                },
                hide: function (el) {
                    $(this).slideUp(el);
                }
            });

            @if(!empty($jenis))

            $repeater.setList({!! json_encode($pengkhususan) !!})
            @endif

        });
    </script>

@endpush
