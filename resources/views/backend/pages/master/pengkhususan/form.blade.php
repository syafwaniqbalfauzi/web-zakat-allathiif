@extends('backend.layouts.app')
@section('title','Dash')
@section('pengkhususan','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item "><a href="{{route('jenis.index')}}">Data Pengkhususan Donasi</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$khusus ? "Ubah" : "Tambah"}} Data Pengkhususan Donasi</li>
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{@$khusus ? "Ubah" : "Tambah"}} Data Pengkhususan Donasi</h4>
                            </div>

                            <div class="card-body">
                                <form action="{{@$khusus ? route('pengkhususan.update', $khusus->id) : route('pengkhususan.store')}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$khusus)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="form-group">
                                        <label for="jenis_id">Jenis Donasi</label>
                                        <select name="jenis_id" id="jenis_id" class="form-control">
                                            @foreach($jenis as $row)
                                            <option value="{{$row->id}}" {{@$khusus->jenis_id == $row->id ? 'selected' : ''}}> {{$row->jenis}} </option>
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="pengkhususan">Pengkhususan Donasi</label>
                                        <input id="pengkhususan" name="pengkhususan" type="text" class="form-control"
                                               value="{{old('pengkhususan',@$khusus->pengkhususan)}}">
                                    </div>

                                    <div class="form-button text-right">
                                        <a href="{{route('jenis.index')}}" class="btn btn-warning">Kembali</a>
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {

        });

    </script>

@endpush
