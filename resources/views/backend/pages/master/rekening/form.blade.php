@extends('backend.layouts.app')
@section('title','Dash')
@section('rekening','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item "><a href="{{route('rekening.index')}}">Data Rekening</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$rekening ? "Ubah" : "Tambah"}} Data Rekening</li>
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{@$rekening ? "Ubah" : "Tambah"}} Data Rekening</h4>
                            </div>

                            <div class="card-body">
                                <form action="{{@$rekening ? route('rekening.update', $rekening->id) : route('rekening.store')}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$rekening)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="form-group">
                                        <label for="nama_bank">Nama Bank</label>
                                        <input id="nama_bank" name="nama_bank" type="text" class="form-control"
                                           value="{{old('nama_bank',@$rekening->nama_bank)}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="kode_bank">Kode Bank</label>
                                        <input id="kode_bank" name="kode_bank" type="text" class="form-control"
                                           value="{{old('kode_bank',@$rekening->kode_bank)}}">

                                    </div>
                                    <div class="form-group">
                                        <label for="no_rekening">Nomor Rekening</label>
                                        <input id="no_rekening" name="no_rekening" type="text" class="form-control"
                                               value="{{old('no_rekening',@$rekening->no_rekening)}}">

                                    </div>
                                    <div class="form-group">
                                        <label for="atas_nama">Atas Nama</label>
                                        <input id="atas_nama" name="atas_nama" type="text" class="form-control"
                                           value="{{old('atas_nama',@$rekening->atas_nama)}}">
                                    </div>

                                    <div class="form-button text-right">
                                        <a href="{{route('rekening.index')}}" class="btn btn-warning">Kembali</a>
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {

        });

    </script>

@endpush
