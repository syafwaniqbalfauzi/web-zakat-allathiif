@extends('backend.layouts.app')
@section('title','Dash')
@section('donasi','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item "><a href="{{route('donasi.index')}}">Data Donasi Masuk</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$donatur ? "Ubah" : "Tambah"}} Data Donasi Masuk</li>
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{@$donatur ? "Ubah" : "Tambah"}} Data Donasi</h4>
                            </div>

                            <div class="card-body">
                                <form action="{{@$donatur ? route('donatur.update', $donatur->id) : route('donatur.store')}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$donatur)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="sapaan">Sapaan Donatur</label>
                                                <select name="sapaan" id="sapaan" class="form-control">
                                                    <option value="Bapak" {{@$donatur->sapaan == 'Bapak' ? 'selected' : ''}}>Bapak</option>
                                                    <option value="Ibu" {{@$donatur->sapaan == 'Ibu' ? 'selected' : ''}}>Ibu</option>
                                                    <option value="Saudara" {{@$donatur->sapaan == 'Saudara' ? 'selected' : ''}}>Saudara</option>
                                                    <option value="Saudari" {{@$donatur->sapaan == 'Saudari' ? 'selected' : ''}}>Saudari</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nama">Nama Donatur</label>
                                                <input type="text" class="form-control" name="nama" id="nama" value="{{old('nama',@$donatur->nama)}}">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email">Email Donatur</label>
                                                <input type="email" class="form-control" name="email" id="email" value="{{old('email',@$donatur->email)}}">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="telepon">No. Telepon Donatur</label>
                                                <input type="number" class="form-control" name="telepon" id="telepon" min="0" value="{{old('telepon',@$donatur->telepon)}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email">Alamat Donatur</label>
                                                <textarea name="alamat" id="alamat" class="form-control" cols="30" rows="10">{{{old('alamat', @$donatur->alamat)}}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-button text-right">
                                        <a href="{{route('donatur.index')}}" class="btn btn-warning">Kembali</a>
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('[name=jenis_id]').select2({
                theme: 'bootstrap4',
                placeholder: '- Pilih Jenis Donasi -',
                ajax: {
                    url: '{{ route('jenis.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });
            $('[name=pengkhususan_id]').select2({
                placeholder: '- Pilih Pengkhususan -',
                ajax: {
                    url: '{{ route('pengkhususan.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            jenis_id: $('[name=jenis_id]').val(),
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });

            $("#jenis_id").on('change',function ()
            {
                $('#pengkhususan_id').prop('disabled', false);
            });
        });

    </script>

@endpush
