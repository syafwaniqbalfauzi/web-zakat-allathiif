@extends('backend.layouts.app')
@section('title','Dash')
@section('donasi','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item "><a href="{{route('donasi.index')}}">Data Donasi Masuk</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{@$donasi ? "Ubah" : "Tambah"}} Data Donasi Masuk</li>
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{@$donasi ? "Ubah" : "Tambah"}} Data Donasi</h4>
                            </div>

                            <div class="card-body">
                                <form action="{{@$donasi ? route('donasi.update', $donasi->id) : route('donasi.store')}}"  method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if(@$donasi)
                                        {{method_field('patch')}}
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <a href="{{route('donatur.create')}}" class="btn btn-sm btn-success ">Tambah Donatur</a>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="donatur_id">Nama Donatur</label>
                                                <select name="donatur_id" id="donatur_id" class="form-control">
                                                    @if(@$donasi)
                                                        <option value="{{$donasi->donatur_id}}">{{$donasi->donatur->nama}}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email">Email Donatur</label>
                                                <input type="email" class="form-control" name="" id="email" value="{{old('email', @$donasi ? @$donasi->donatur->email : '')}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="telepon">No. Telepon Donatur</label>
                                                <input type="number" class="form-control" name="" id="telepon" min="0" value="{{old('telepon', @$donasi ? @$donasi->donatur->telepon : '')}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="alamat">Alamat</label>
                                                <textarea class="form-control" name="" id="alamat" required="" readonly>{{old('alamat', @$donasi ? @$donasi->donatur->alamat : '')}}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <h6>Detail Donasi</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="transfer">Status</label>
                                                <select name="transfer" id="transfer" class="form-control">
                                                    <option value="">- Pilih Status Donasi -</option>
                                                    <option value="0" {{old('transfer', @$donasi->transfer ) == 0 ? 'selected' : ''}}>Pembayaran Langsung</option>
                                                    <option value="1" {{old('transfer', @$donasi->transfer ) == 1 ? 'selected' : ''}}>Belum Transfer</option>
                                                    <option value="2" {{old('transfer', @$donasi->transfer ) == 2 ? 'selected' : ''}}>Transfer Terkonfirmasi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="rekening_id">Rekening</label>
                                                <select name="rekening_id" id="rekening_id" class="form-control">
                                                    @if(@$donasi)
                                                        <option value="{{$donasi->rekening_id}}">{{$donasi->rekening->nama_bank .' - '.$donasi->rekening->kode_bank.' '.$donasi->rekening->no_rekening }}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="tanggal_donasi">Tanggal Donasi</label>
                                                <input type="date" class="form-control" name="tanggal_donasi" id="tanggal_donasi" value="{{old('alamat', @$donasi ? @$donasi->tanggal_donasi : '')}}">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="tanggal_transfer">Tanggal Transfer</label>
                                                <input type="date" class="form-control" name="tanggal_transfer" id="tanggal_transfer" value="{{old('alamat', @$donasi ? @$donasi->tanggal_donasi : '')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="total_nominal">Total Nominal Donasi</label>
                                                <input type="number" class="form-control mask" name="total_nominal" id="total_nominal" value="{{@$donasi ? $donasi->total_nominal : ''}}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group custom-file">
                                                <label for="bukti_transfer">Bukti Transfer</label>
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="bukti_transfer" id="bukti_transfer">
                                                    <label class="custom-file-label" for="bukti_transfer">Cari Bukti Transfer</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="pk-repeater repeater">
                                                <div class="form-group">
                                                    <div class="text-left">
                                                        <button type="button" class="btn btn-sm btn-primary" data-repeater-create type="button">
                                                            <i class="fa fa-plus"></i> Tambah Jenis Donasi
                                                        </button>
                                                    </div>
                                                </div>
                                                <div data-repeater-list="khusus">
                                                    @if(@$donasi)
                                                        @foreach($donasi->donasi_detail as $row)
                                                            <div data-repeater-item>
                                                                <div class="form-group">
                                                                    <div class="row mt-0">
                                                                        <div class="col-4">
                                                                            <div class="form-group">
                                                                                <label for="pengkhususan_id">Pengkhususan</label>
                                                                                <select name="pengkhususan_id" id="" class="form-control pengkhususan " >
                                                                                    <option value="{{$row->pengkhususan_id}}">{{$row->pengkhususan->jenis->jenis.' - '.$row->pengkhususan->pengkhususan}}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-3">
                                                                            <div class="form-group">
                                                                                <label for="nominal">Nominal Donasi</label>
                                                                                <input type="number" class="form-control  nominal" name="nominal" id="nominal" value="{{$row->nominal}}" >
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-1">
                                                                            <div class="form-group" style="margin-top: 30px">
                                                                                <button type="button" class="btn btn-danger btn-hapus-repeater " data-repeater-delete><i class="fa fa-trash"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        @endforeach

                                                    @else
                                                        <div data-repeater-item>
                                                            <div class="form-group">
                                                                <div class="row mt-0">
                                                                    <div class="col-4">
                                                                        <div class="form-group">
                                                                            <label for="pengkhususan_id">Pengkhususan</label>
                                                                            <select name="pengkhususan_id" id="" class="form-control pengkhususan " >
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="form-group">
                                                                            <label for="nominal">Nominal Donasi</label>
                                                                            <input type="number" class="form-control  nominal" name="nominal" id="nominal"  >
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-1">
                                                                        <div class="form-group" style="margin-top: 30px">
                                                                            <button type="button" class="btn btn-danger " data-repeater-delete><i class="fa fa-trash"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-button text-right">
                                        <a href="{{route('donasi.index')}}" class="btn btn-warning">Kembali</a>
                                        <button class="btn btn-primary" type="submit" id="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script src="{{asset('assets/backend/')}}/js/jquery.repeater.min.js"></script>


    <script>

        $(document).ready(function() {
            var total_nominal = 0;

            $('.mask').mask('000.000.000.000.000,00', {reverse: false});

            $('#submit').on('click', function (){
                $('.mask').unmask();
            })

            $repeater = $('.repeater').repeater({
                show: function () {
                    $(this).slideDown();
                    $('.nominal').keyup( function (){
                        hitungNominal();
                    });
                    $('.select2-input').last().html('<select name="khusus[][pengkhususan_id]" id="" class="form-control pengkhususan "></select>');
                    $('.pengkhususan').select2({
                        placeholder: '- Pilih Pengkhususan -',
                        ajax: {
                            url: '{{ route('pengkhususan.getList') }}',
                            type: 'POST',
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                    page: params.page || 1,
                                    jenis_id: $('[name=jenis_id]').val(),
                                    _token: '{{ csrf_token() }}',
                                }
                                return query;
                            }
                        }
                    });
                },
                hide: function (el) {
                    $(this).slideUp(el);
                    $('.btn-hapus-repeater').on('click', function (){
                        setInterval(function(){
                            var nominal_list = $('.nominal');
                            total_nominal = 0;
                            for (let i = 0;i < nominal_list.length ;i++){
                                total_nominal+=parseInt($(nominal_list[i]).val());
                            }
                            $('#total_nominal').val(total_nominal)
                        }, 1);
                    });
                }
            });

{{--            @if(!empty($donasi))--}}
{{--                $repeater.setList({!! json_encode($donasi_detail) !!});--}}
{{--            @endif--}}

            $(document).on('change','#donatur_id',function (e) {
                e.preventDefault();
                var id = $(this).val();
                $.ajax({
                    'type': 'get',
                    'url': '{{url('admin/donatur/')}}/'+id,
                    success: function (res) {
                        var data = res.data;
                        $('#email').val(data.email);
                        $('#telepon').val(data.telepon);
                        $('#alamat').val(data.alamat);

                    }
                });
            });
            $('.btn-hapus-repeater').on('click', function (){
                setInterval(function(){
                    var nominal_list = $('.nominal');
                    total_nominal = 0;
                    for (let i = 0;i < nominal_list.length ;i++){
                        total_nominal+=parseInt($(nominal_list[i]).val());
                    }
                    $('#total_nominal').val(total_nominal)
                }, 1);
            });

            $('.nominal').keyup( function (){
                hitungNominal();
            });


            $('[name=donatur_id]').select2({
                theme: 'bootstrap4',
                placeholder: '- Pilih Nama -',
                ajax: {
                    url: '{{ route('donatur.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });

            $('[name=rekening_id]').select2({
                theme: 'bootstrap4',
                placeholder: '- Pilih Rekening -',
                ajax: {
                    url: '{{ route('rekening.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });

             $('.pengkhususan').select2({
                placeholder: '- Pilih Pengkhususan -',
                ajax: {
                    url: '{{ route('pengkhususan.getList') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            page: params.page || 1,
                            _token: '{{ csrf_token() }}',
                        }
                        return query;
                    }
                }
            });


            function hitungNominal(){
               var nominal_list = $('.nominal');
               total_nominal = 0;
               for (let i = 0;i<nominal_list.length;i++){
                   total_nominal+=parseInt($(nominal_list[i]).val());
               }
               $('#total_nominal').val(total_nominal)
            }
        });


    </script>

@endpush
