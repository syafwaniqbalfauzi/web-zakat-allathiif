@extends('backend.layouts.app')
@section('title','Dash')
@section('donasi','active')

@push('styles')
    <style>

    </style>
@endpush
@section('content')

    <div class="main-content">
        <sectiion>
            <div class="section-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Data Donasi Masuk</li>

                        <li class="breadcrumb-item"><a href="#"></a></li>
                        {{--                        <li class="breadcrumb-item active"><a href="#">Data Jenis</a></li>--}}
{{--                        <li class="breadcrumb-item active" aria-current="page">Data</li>--}}
                    </ol>
                </nav>
            </div>
        </sectiion>
        <section class="section">
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Data Donasi Masuk</h4>
                                <div class="card-header-action">
                                    <a href="{{ route('donasi.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-jenis">
                                        <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Kode Ver.</th>
                                            <th>Nama Donatur</th>
                                            <th>Telepon</th>
                                            <th>Rekening</th>
                                            <th>Detail Donasi</th>
                                            <th>Total Donasi</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function() {

            $(document).on('click','.btn-bukti', function (e) {
                e.preventDefault();
                let id = $(this).data('id');

                $.ajax({
                    'type': 'post',
                    'url': '{{url('admin/donasi/download-bukti')}}',
                    'data':{
                        'id' : id,
                        '_token' :'{{csrf_token()}}'

                    },
                    success: function (response) {
                    },
                    error:function (error) {
                        console.log(error)
                    }
                });

            });

            $(document).on('click','.btn-status', function (e) {
                e.preventDefault();
                let id = $(this).data('id');
                let status = $(this).data('status');
                let stat = '';
                if(status == 0){
                    stat = 'Pembayaran Langsung';
                }else if(status == 1){
                    stat = 'Menunggu Konfirmasi';
                }else{
                    stat = 'Terkonfirmasi';
                }
                swal({
                    title: 'Apakah Anda Yakin?',
                    text: 'Status donasi anda akan diubah menjadi '+stat+'!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willChange) => {
                        if (willChange) {
                            $.ajax({
                                'type': 'post',
                                'url': '{{url('admin/donasi/ganti-status')}}',
                                'data':{
                                    'id' : id,
                                    'status' :status,
                                    '_token' :'{{csrf_token()}}'

                                },
                                success: function (response) {
                                    // $('.nama_produk').html(response.pembelian_barang.nama_barang);

                                    if (response == 1){
                                        location.reload();
                                    }

                                }
                            });
                        }
                    });

            });

            $('#table-jenis').DataTable( {
                deferRender: true,
                serverSide: true,
                processing: true,
                stateSave: true,
                scrollX: false,
                ajax: {
                    url: '{!! route('donasi.get') !!}',
                    type: 'POST',
                    data: function (e) {
                        e._token = '{{ csrf_token() }}';
                        return e;
                    }
                }
            });

            $(document).on('click','.btn-del', function () {
                var id = $(this).data('id');
                swal({
                    title: 'Apakah Anda Yakin?',
                    text: 'Data yang telah dihapus tidak bisa dikembalikan!',
                    icon: 'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "delete",
                            url: "{{ url('admin/donasi') }}/"+id,
                            data: {
                                '_token' : '{{ csrf_token() }}'
                            },
                            success: function (response) {
                                if(response == 1){
                                    swal('Data berhasil dihapus!', {
                                        icon: 'success',
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                }else{
                                    swal('Data gagal dihapus!', {
                                        icon: 'error',
                                    });
                                }
                            }
                        });
                    }
                });
            });
        });

    </script>

@endpush
