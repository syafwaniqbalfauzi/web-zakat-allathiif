<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{url('admin')}}"> <span
                    class="logo-name">DKM Al Lathiif</span>
            </a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture">
                <img alt="image" src="{{asset('assets/backend/')}}/img/userbig.png">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">Sarah Smith</div>
                <div class="user-role">Administrator</div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li><a class="nav-link active" href="#"><i class="far fa-chart-bar"></i><span>Dashboard</span></a></li>

            <li class="dropdown @yield('donasi')">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-wallet"></i><span>Donasi</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{route('donasi.index')}}">Donasi Masuk</a></li>
                    <li><a class="nav-link" href="index2.html">Donasi Terkonfirmasi</a></li>
                </ul>
            </li>
            <li class="@yield('donatur')"><a class="nav-link active" href="{{route('donatur.index')}}"><i class="fas fa-money-check"></i><span>Donatur</span></a></li>


            <li class="menu-header">Data Master</li>

            <li class="@yield('rekening')"><a class="nav-link active" href="{{route('rekening.index')}}"><i class="fas fa-money-check"></i><span>Rekening</span></a></li>
            <li class="@yield('jenis')"> <a class="nav-link" href="{{route('jenis.index')}}"><i class="fas fa-fax"></i><span>Jenis Donasi</span></a></li>

        </ul>
    </aside>
</div>
