<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::group(['middleware' => ['web']], function () {

    Auth::routes();
    /*--------------------------ADMIN--------------------------------------*/
    Route::prefix('admin')->middleware('auth')->group(function () {

        Route::get('/', function () {
            return view('backend.pages.dashboard');
        });

        Route::resource('rekening','Admin\RekeningController');
        Route::post('rekening/get', 'Admin\RekeningController@getAllRekening')->name('rekening.get');
        Route::patch('rekening/{rekening}', 'Admin\RekeningController@store')->name('rekening.update');
        Route::post('rekening/getList', 'Admin\RekeningController@getRekeningList')->name('rekening.getList');

        Route::resource('jenis','Admin\JenisController');
        Route::post('jenis/get', 'Admin\JenisController@getAllJenis')->name('jenis.get');
        Route::patch('jenis/{jeni}', 'Admin\JenisController@store')->name('jenis.update');
        Route::post('jenis/getList', 'Admin\JenisController@getJenisList')->name('jenis.getList');

        Route::resource('pengkhususan','Admin\PengkhususanController');
        Route::post('pengkhususan/get', 'Admin\PengkhususanController@getAllKhusus')->name('pengkhususan.get');
        Route::patch('pengkhususan/{pengkhususan}', 'Admin\PengkhususanController@store')->name('pengkhususan.update');
        Route::post('pengkhususan/getList', 'Admin\PengkhususanController@getPengkhususanList')->name('pengkhususan.getList');

        Route::resource('donasi','Admin\DonasiController');
        Route::post('donasi/get', 'Admin\DonasiController@getAllDonasi')->name('donasi.get');
        Route::post('donasi/ganti-status', 'Admin\DonasiController@gantiStatus');
        Route::post('donasi/download-bukti', 'Admin\DonasiController@downloadBukti');

        Route::resource('donatur','Admin\DonaturController');
        Route::post('donatur/get', 'Admin\DonaturController@getAllDonatur')->name('donatur.get');
        Route::post('donatur/getList', 'Admin\DonaturController@getDonaturList')->name('donatur.getList');

    });
    /*--------------------------FRONTEND--------------------------------------*/

    Route::get('/', function (){
        return view('welcome');
    });
    Route::get('donasi', 'Front\HomeController@index');
    Route::resource('fdonatur', 'Front\DonaturController');
    Route::resource('fdonatur', 'Front\DonaturController');
    Route::post('home-donatur/getList', 'Front\DonaturController@getDonaturList');
    Route::get('home-donatur/{id}', 'Front\DonaturController@getDonatur');
    Route::post('home-pengkhususan/getList', 'Front\HomeController@getPengkhususanList')->name('home-pengkhususan.getList');


});

